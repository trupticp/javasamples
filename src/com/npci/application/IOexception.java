package com.npci.application;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class IOexception {

	public static void main(String[] args) {
		
		IOexception obj = new IOexception();
		obj.testMethod();
	}
public FileInputStream testMethod() {
		File file = new File("test.txt");
		FileInputStream fileInputStream = null;
		try{
		fileInputStream = new FileInputStream(file);
		fileInputStream.read();
		}catch (IOException e){
		e.printStackTrace();
		}
		finally{
		try{
		if (fileInputStream != null){
		fileInputStream.close();
		}
		}catch (IOException e){
		e.printStackTrace();
		}
		}
		return fileInputStream;
		}
}

