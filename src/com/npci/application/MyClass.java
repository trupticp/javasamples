package com.npci.application;

abstract public class MyClass {
       //complete method
	
	public void calc() {
		System.out.println("calculating results");
		
	}
	
	//abstract method
	abstract public void launchRocket();
	
}




//abstrct class cannot be instantiated


class start {
	public static void main(String[] args) {
		MyChild myChild = new MyChild();
		
		myChild.launchRocket();
		myChild.calc();
	}
}
