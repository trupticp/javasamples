package com.npci.application;
class A{}

class B extends A{}

public class Parent {
	// overridden method
	//public void m1()
	public A m1()
	{
		System.out.println("I am m1 Method of parent");
		return new A();
	}

}


class Child extends Parent{
	//overriding method
	//public void m1() 
	public B m1(){
		System.out.println("i am m1 of child");
		return new B();
	}
}